#-------------------------------------------------------------------------------
# Name:        plot-speed-time.py
# Purpose:     To display a speed/time graph used to calculate area
#              for physics notes.
#
# Author:      Aaron Singline
#
# Created:     15/01/2014
# Copyright:   (c) Aaron Singline 2014
# Licence:     GPL v3
#-------------------------------------------------------------------------------


# matplotlib is pretty important for these projects.
from matplotlib.font_manager import FontProperties
# Also pylab comes from matplotlib and
# not the other project of the same name.
from pylab import *

# Set the graph as 10 units each axis
x = arange( 10 )
y = x

# For each unit have a grid line.
ax = gca()
ax.xaxis.set_major_locator( MultipleLocator(1.0) )
ax.yaxis.set_major_locator( MultipleLocator(1.0) )
ax.grid( which='major', axis='x', linewidth=0.75, linestyle='-', color='0.75' )
ax.grid( which='major', axis='y', linewidth=0.75, linestyle='-', color='0.75' )


font = FontProperties()
font.set_family('serif')
font.set_style('italic')
font.set_size('12')
ax.text(4, 1.25, "a", fontproperties=font, va="bottom")
ax.text(4, 3.25, "b", fontproperties=font, va="bottom")

plot(x, y)

# Set the labels
ylabel( 'speed (meters per second)' )
xlabel( 'time (seconds)' )

# Draw the areas down to the time axis
# Draw the triangle first
fill( [3,5,5,3], [3,3,5,3], 'b', alpha=0.5, edgecolor='r' )
# Now draw the rectangle
fill( [3,5,5,3], [0,0,3,3], 'g', alpha=0.5, edgecolor='r' )

# Now show the graph
show()
