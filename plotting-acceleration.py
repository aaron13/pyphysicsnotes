#-------------------------------------------------------------------------------
# Name:        plotting-acceleration.py
# Purpose:     General purpose template.
#
# Author:      Aaron Singline
#
# Created:     21/01/2014
# Copyright:   (c) Aaron Singline 2014
# Licence:     GPL v3
#-------------------------------------------------------------------------------

# Pylab comes from matplotlib and
# not the other project of the same name.
from pylab import *

# Set the graph as 10 units each axis
x = arange( 10 )
y = x

# For each unit have a grid line.
ax = gca()
ax.xaxis.set_major_locator( MultipleLocator(1.0) )
ax.yaxis.set_major_locator( MultipleLocator(1.0) )
ax.grid( which='major', axis='x', linewidth=0.75, linestyle='-', color='0.75' )
ax.grid( which='major', axis='y', linewidth=0.75, linestyle='-', color='0.75' )

step = 1
steps = 0
while steps < 10:
    if steps == 0:
        x1 = 0
        y1 = 0
    else:
        x1 = steps
        y1 = x1 / 2
    plot(x1, y1)
    steps = steps + step

# Set the labels
ylabel( 'speed (meters per second)' )
xlabel( 'time (seconds)' )


# Now show the graph
show()
